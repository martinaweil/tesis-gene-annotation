
library("ggplot2")

#analisis de redundancia
blastx_sp <- read.delim("~/tesis-gene-annotation/Especies/Dant_sinfiltrar/blastx.outfmt6", header=FALSE) 
blastx_dros <- read.delim("~/tesis-gene-annotation/Especies/Dant_basedatos_drosophila/blastx.outfmt6", header=FALSE)
lista_dros<-unique(blastx_dros$V2)
lista_sp<-unique(blastx_sp$V2)     #veo la cantidad de accesions unicos 

frec_dros<-data.frame(table(blastx_dros$V2))   #veo la frecuencia de cada accesion
frec_sp<-data.frame(table(blastx_sp$V2))

frec_dros_filtrado<-frec_dros[frec_dros$Freq>50,] #filtro la tabla. solo aquellos con frec>30
frec_sp_filtrado<-frec_sp[frec_sp$Freq>50,]

##################################################################################################################
#GRÁFICOS#
##################################################################################################################
D<-ggplot(frec_dros_filtrado, aes(x=reorder(Var1, -Freq), Freq)) +
  geom_point(size = 2)

D + theme(axis.text.x = element_text(face = "bold", color = "#993333", size = 8, angle = 50,hjust = 1))+xlab("accesion ID")+ggtitle("SP Drosophila")
         
SP<-ggplot(frec_sp_filtrado, aes(x=reorder(Var1, -Freq), Freq)) +
  geom_point(size = 2)

SP + theme(axis.text.x = element_text(face = "bold", color = "#993333", size = 8, angle = 50,hjust = 1))+xlab("accesion ID")+ggtitle("Swissprot")


#################################################################################################################
#BUSCO LOS ACCESIONS CON MAYOR NUMERO DE CONTIGS 

dros_orden <- frec_dros_filtrado[order(frec_dros_filtrado$Freq,decreasing = TRUE),]
sp_orden <- frec_sp_filtrado[order(frec_sp_filtrado$Freq,decreasing = TRUE),]

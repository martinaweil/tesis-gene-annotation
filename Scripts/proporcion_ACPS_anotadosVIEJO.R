setwd("~/Mar/tesis-gene-annotation/Especies/Dant")
Dant <- read.table("trinotate_annotation_report.xls", header=T, sep="\t", quote="")
Dant_acps<-read.table("Dant_factor_labeling.txt", header=F,sep="\t",quote="")


setwd("~/Mar/tesis-gene-annotation/Especies/Dbuz")
Dbuz <- read.table("trinotate_annotation_report.xls", header=T, sep="\t", quote="")
Dbuz_acps<-read.table("Dbuz_factor_labeling.txt", header=F,sep="\t",quote="")
setwd("~/Mar/tesis-gene-annotation/Especies/Dbor")
Dbor <- read.table("trinotate_annotation_report.xls", header=T, sep="\t", quote="")
Dbor_acps<-read.table("Dbor_factor_labeling.txt", header=F,sep="\t",quote="")
setwd("~/Mar/tesis-gene-annotation/Especies/Dkoe")
Dkoe <- read.table("trinotate_annotation_report.xls", header=T, sep="\t", quote="")
Dkoe_acps<-read.table("Dkoe_factor_labeling.txt", header=F,sep="\t",quote="")


proporcion_anotados <- function(outputTrinotate, fact_l) {
  niveles <- levels(outputTrinotate$gene_id)
  N <- length(niveles)
  filtro <- logical(N)
  for(i in 1:N) {
    if (niveles[i] %in% fact_l$V2){
      print(paste0("progress ", i*100/N, "%"))
      pos <- which(outputTrinotate$gene_id == niveles[i])
      M <- length(pos)
      for (j in 1:M) {
        if(outputTrinotate$gene_ontology_blast[pos[j]] != "." || outputTrinotate$gene_ontology_pfam[pos[j]] != ".") {
          filtro[i] <- T
          }
        }
      } else { print(paste0("progress ", i*100/N, "%"))
    }
  }
  annots <- sum(filtro)
  p <- annots/length(fact_l$V2)
  print(p)
  return(p)

}

p_Dant<-proporcion_anotados(Dant, Dant_acps)
p_Dbuz<-proporcion_anotados(Dbuz, Dbuz_acps)
p_Dbor<-proporcion_anotados(Dbor, Dbor_acps)
p_Dkoe<-proporcion_anotados(Dkoe, Dkoe_acps)




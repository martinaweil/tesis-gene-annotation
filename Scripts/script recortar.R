Dkoe<-read.table("AG.GOseq.enriched",header=T,sep="\t",quote="")

Dkoe<-Dkoe[Dkoe$ontology == 'MF',]


library(dplyr)

Dkoe<-Dkoe%>% filter(over_represented_pvalue <= 1e-03)

write.table(Dkoe, "dkoerecortada.txt", append = FALSE, quote=F, sep = " ", dec = ".", row.names = F, col.names = T)

library(VennDiagram)
dant <- read.csv("~/tesis-gene-annotation/Especies/Dant_sinfiltrar/REVIGO_mf.csv")
dbor <- read.csv("~/tesis-gene-annotation/Especies/Dbor_sinfiltrar/REVIGO_mf.csv")
dkoe <- read.csv("~/tesis-gene-annotation/Especies/Dkoe_sinfiltrar/REVIGO_mf.csv")
dbuz <- read.csv("~/tesis-gene-annotation/Especies/Dbuz_sinfiltrar/REVIGO_mf.csv")
dmel <- read.csv("~/tesis-gene-annotation/Especies/Dmel/molecularfunction.csv")



dant <- read.csv("~/tesis-gene-annotation/Especies/Dant_sinfiltrar/")
dbor <- read.csv("~/tesis-gene-annotation/Especies/Dbor_basedatos_drosophila/REVIGO_biologicalprocess.csv")
dkoe <- read.csv("~/tesis-gene-annotation/Especies/Dkoe_basedatos_drosophila/REVIGO_biologicalprocesses.csv")
dbuz <- read.csv("~/tesis-gene-annotation/Especies/Dbuz_basedatos_drosophila/REVIGO_biologicalprocess.csv")
dmel <- read.csv("~/tesis-gene-annotation/Especies/Dmel/biologicalprocess.csv")

dant <- read.csv("~/tesis-gene-annotation/Especies/Dant_basedatos_drosophila/REVIGO_celularcomponent.csv")
dbor <- read.csv("~/tesis-gene-annotation/Especies/Dbor_basedatos_drosophila/REVIGO_celularcomponent.csv")
dkoe <- read.csv("~/tesis-gene-annotation/Especies/Dkoe_basedatos_drosophila/REVIGO_celularprocesses.csv")
dbuz <- read.csv("~/tesis-gene-annotation/Especies/Dbuz_basedatos_drosophila/REVIGO_celularcomponent.csv")
dmel <- read.csv("~/tesis-gene-annotation/Especies/Dmel/cellularcomp.csv")

dant<-dant[dant$eliminated==0,]
dbor<-dbor[dbor$eliminated==0,]
dbuz<-dbuz[dbuz$eliminated==0,]
dkoe<-dkoe[dkoe$eliminated==0,]
dmel<-dmel[dmel$eliminated==0,]

Dant<-dant$description
Dbor<-dbor$description
Dbuz<-dbuz$description
Dkoe<-dkoe$description
Dmel<-dmel$description


intersect(Dkoe,intersect(Dbuz,intersect(Dant,Dbor)))

intersect(Dmel,intersect(Dkoe,intersect(Dbuz,intersect(Dant,Dbor))))

venn.diagram(
  x=list(Dant,Dbor,Dkoe,Dbuz),
  category.names = c("D. Antonietae","D borborema", "D koepferae", "D buzzatii"),
  filename = "biolproc.png"
)




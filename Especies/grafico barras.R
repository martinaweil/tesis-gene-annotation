# Create data GENES TOTALES 

data <- matrix(c(15495,20472,14670,24397,13084,17406,13776) , nrow=2)
colnames(data) <- c("D ant","D bor","D buz","D koe")
rownames(data) <- c("genes anotados","genes sin anotar")

# create color palette:
library(broman)
coul <- c(brocolors("crayons")["Inchworm"],brocolors("crayons")["Scarlet"])

# Transform this data in %
data_percentage <- apply(data, 2, function(x){x*100/sum(x,na.rm=T)})

# Make a stacked barplot--> it will be in %!
plot<-barplot(data_percentage, col=coul , border="black", xlab="especie", ylab = "porcentaje", main = "Genes totales",legend=rownames(data))


#############################################################################3
#APCS

# Create data

data <- matrix(c(92,40,62,36,149,135,163,109) , nrow=2)
colnames(data) <- c("D ant","D bor","D buz","D koe")
rownames(data) <- c("genes anotados","genes sin anotar")

# create color palette:
library(broman)
coul <- c(brocolors("crayons")["Inchworm"],brocolors("crayons")["Scarlet"])

# Transform this data in %
data_percentage <- apply(data, 2, function(x){x*100/sum(x,na.rm=T)})

# Make a stacked barplot--> it will be in %!
plot<-barplot(data_percentage, col=coul , border="black", xlab="especie", ylab = "porcentaje", main = "Genes ACPS")
